package com.utm.scheduler.mapper;

import com.utm.scheduler.dto.TeacherTimeSendDto;
import com.utm.scheduler.entity.TeacherTimeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = DaysMapper.class)
public interface TeacherMapper {
    TeacherMapper INSTANCE = Mappers.getMapper(TeacherMapper.class);

    //    @Mapping(source = "getDays", target = "days")
    TeacherTimeEntity dtoToEntity(TeacherTimeSendDto teacherTimeSendDto);


    TeacherTimeSendDto entityToDto(TeacherTimeEntity teacherTimeEntity);
}
