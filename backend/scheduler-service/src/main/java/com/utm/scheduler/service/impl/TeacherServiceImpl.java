package com.utm.scheduler.service.impl;

import com.utm.scheduler.dto.DurationDto;
import com.utm.scheduler.dto.TeacherTimeSendDto;
import com.utm.scheduler.dto.TimeDto;
import com.utm.scheduler.entity.TeacherTimeEntity;
import com.utm.scheduler.entity.WorkingHoursDurationEntity;
import com.utm.scheduler.mapper.DaysMapper;
import com.utm.scheduler.mapper.TeacherMapper;
import com.utm.scheduler.repository.TeacherTimeEntityRepository;
import com.utm.scheduler.service.TeacherService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {
    private final TeacherMapper teacherMapper = TeacherMapper.INSTANCE;
    private final DaysMapper daysMapper = DaysMapper.INSTANCE;
    private final TeacherTimeEntityRepository teacherTimeEntityRepository;

    public TeacherServiceImpl(TeacherTimeEntityRepository teacherTimeEntityRepository) {
        this.teacherTimeEntityRepository = teacherTimeEntityRepository;
    }

    @Override
    public TeacherTimeSendDto createTeacherTime(TeacherTimeSendDto teacherTimeSendDto) {
        TeacherTimeEntity teacherTimeEntity = teacherMapper.dtoToEntity(teacherTimeSendDto);
        teacherTimeEntity.setWorkingHoursDurationEntities(getWorkingHours(teacherTimeSendDto));

        teacherTimeEntity = teacherTimeEntityRepository.save(teacherTimeEntity);
        //todo: add all working times, etc
        return teacherMapper.entityToDto(teacherTimeEntity);
    }

    @Override
    @Transactional
    public TimeDto getTime(int id, LocalDate startDate) {
        TeacherTimeEntity timeEntity = teacherTimeEntityRepository.findAllByIdAndStartScheduleDateIsBefore(id, startDate);

        TimeDto timeDto = new TimeDto();
        timeDto.setDays(daysMapper.entityToDto(timeEntity.getDays()));
        timeDto.setDurations(timeEntity.getWorkingHoursDurationEntities().stream().map(x -> new DurationDto(x.getStartWorkingTime(), x.getEndWorkingTime())).collect(Collectors.toList()));
        return timeDto;

    }

    private List<WorkingHoursDurationEntity> getWorkingHours(TeacherTimeSendDto teacherTimeSendDto) {
        List<WorkingHoursDurationEntity> list = new LinkedList<>();
        WorkingHoursDurationEntity workingHoursDurationEntity = new WorkingHoursDurationEntity();
        workingHoursDurationEntity.setStartWorkingTime(teacherTimeSendDto.getStartWorkingTime());
        if (teacherTimeSendDto.getStartBreakTime() != null && teacherTimeSendDto.getEndBreakTime() != null) {
            workingHoursDurationEntity.setEndWorkingTime(teacherTimeSendDto.getStartBreakTime());
            list.add(workingHoursDurationEntity);

            workingHoursDurationEntity = new WorkingHoursDurationEntity();
            workingHoursDurationEntity.setStartWorkingTime(teacherTimeSendDto.getEndBreakTime());

        }
        workingHoursDurationEntity.setEndWorkingTime(teacherTimeSendDto.getEndWorkingTime());
        list.add(workingHoursDurationEntity);
        return list;
    }


}
