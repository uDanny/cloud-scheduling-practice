package com.utm.scheduler.entity;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class DaysEntity {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "days_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column
    private boolean MO;
    @Column
    private boolean TU;
    @Column
    private boolean WE;
    @Column
    private boolean TH;
    @Column
    private boolean FR;
    @Column
    private boolean ST;
    @Column
    private boolean SU;

}
