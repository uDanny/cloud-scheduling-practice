package com.utm.scheduler.mapper;

import com.utm.scheduler.dto.DaysDto;
import com.utm.scheduler.entity.DaysEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DaysMapper {
    DaysMapper INSTANCE = Mappers.getMapper(DaysMapper.class);

    DaysEntity dtoToEntity(DaysDto daysDto);

    DaysDto entityToDto(DaysEntity daysEntity);

    List<DaysEntity> dtoToEntity(List<DaysDto> daysDto);

    List<DaysDto> entityToDto(List<DaysEntity> daysEntities);
}
