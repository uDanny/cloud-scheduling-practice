package com.utm.scheduler.controller;

import com.utm.scheduler.dto.TeacherTimeSendDto;
import com.utm.scheduler.dto.TimeDto;
import com.utm.scheduler.service.TeacherService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
public class ApiController {

    private final TeacherService teacherService;

    public ApiController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/isAlive")
    public String getRoot() {
        return "Welcome from User API";
    }

    @PostMapping("/teacher")
    public TeacherTimeSendDto createTeacherTime(@RequestBody TeacherTimeSendDto teacherTimeSendDto) {
        return teacherService.createTeacherTime(teacherTimeSendDto);
    }

    @GetMapping("/getUserTime/{id}/{startDate}")
    public TimeDto getTime(@PathVariable("id") int id, @PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate) {

        return teacherService.getTime(id, startDate);
    }

}
