package com.utm.scheduler.entity;

import javax.persistence.*;
import java.time.LocalDate;

public class WorkingDaysDurationEntity {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "working_days_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private int id;
    @Column
    private LocalDate startScheduleDate;
    @Column
    private LocalDate endScheduleDate;
}
