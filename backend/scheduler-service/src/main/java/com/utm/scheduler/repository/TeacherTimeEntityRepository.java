package com.utm.scheduler.repository;

import com.utm.scheduler.entity.TeacherTimeEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface TeacherTimeEntityRepository extends JpaRepository<TeacherTimeEntity, Integer> {
    @EntityGraph(attributePaths = {"days", "workingHoursDurationEntities"})
    TeacherTimeEntity findAllByIdAndStartScheduleDateIsBefore(int id, LocalDate localDate);
}
