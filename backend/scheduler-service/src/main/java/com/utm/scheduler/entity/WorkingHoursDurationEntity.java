package com.utm.scheduler.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalTime;
@Data
@Entity
public class WorkingHoursDurationEntity {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "working_hours_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private int id;
    @Column
    private LocalTime startWorkingTime;
    @Column
    private LocalTime endWorkingTime;
}
