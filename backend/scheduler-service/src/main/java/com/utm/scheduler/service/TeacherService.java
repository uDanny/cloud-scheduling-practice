package com.utm.scheduler.service;

import com.utm.scheduler.dto.TeacherTimeSendDto;
import com.utm.scheduler.dto.TimeDto;

import java.time.LocalDate;

public interface TeacherService {

    TeacherTimeSendDto createTeacherTime(TeacherTimeSendDto teacherTimeSendDto);

    TimeDto getTime(int id, LocalDate startDate);
}
