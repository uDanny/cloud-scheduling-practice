package com.utm.scheduler.entity;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
public class TeacherTimeEntity {
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<WorkingHoursDurationEntity> workingHoursDurationEntities;
    @Column
    private LocalDate startScheduleDate;
    @Column
    private LocalDate endScheduleDate;
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private DaysEntity days;
}
