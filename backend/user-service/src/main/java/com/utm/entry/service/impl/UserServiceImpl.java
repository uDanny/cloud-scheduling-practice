package com.utm.entry.service.impl;

import com.utm.entry.dto.DefaultUserDto;
import com.utm.entry.dto.TeacherUser;
import com.utm.entry.entity.user.UserEntity;
import com.utm.entry.mapper.EmployerMapper;
import com.utm.entry.mapper.UserMapper;
import com.utm.entry.repository.UserEntityRepository;
import com.utm.entry.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserEntityRepository userEntityRepository;
    private final UserMapper userMapper = UserMapper.INSTANCE;
    private final EmployerMapper employerMapper = EmployerMapper.INSTANCE;

    public UserServiceImpl(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    public List<DefaultUserDto> getAllUserDetails() {
        List<UserEntity> all = userEntityRepository.findAll();
        return userMapper.entitiesToDtos(all);
    }

    @Override
    public DefaultUserDto createUser(DefaultUserDto defaultUserDto) {
        UserEntity userEntity = userMapper.dtoToEntity(defaultUserDto);
        userEntity = userEntityRepository.save(userEntity);
        return userMapper.entityToDto(userEntity);
    }

    @Override
    public TeacherUser createTeacher(TeacherUser teacherUser) {
        UserEntity userEntity = employerMapper.dtoToEntity(teacherUser);
        userEntity = userEntityRepository.save(userEntity);
        return employerMapper.entityToDto(userEntity);
    }
}
