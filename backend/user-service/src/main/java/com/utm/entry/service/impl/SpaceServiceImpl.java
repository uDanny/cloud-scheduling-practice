package com.utm.entry.service.impl;

import com.utm.entry.dto.SpaceDto;
import com.utm.entry.entity.Space;
import com.utm.entry.mapper.SpaceMapper;
import com.utm.entry.repository.SpaceRepository;
import com.utm.entry.service.SpaceService;
import org.springframework.stereotype.Service;

@Service
public class SpaceServiceImpl implements SpaceService {
    private final SpaceRepository spaceRepository;
    private final SpaceMapper spaceMapper = SpaceMapper.INSTANCE;

    public SpaceServiceImpl(SpaceRepository spaceRepository) {
        this.spaceRepository = spaceRepository;
    }

    @Override
    public SpaceDto createSpace(SpaceDto spaceDto) {
        Space space = spaceRepository.save(spaceMapper.dtoToEntity(spaceDto));
        return spaceMapper.entityToDto(space);
    }
}
