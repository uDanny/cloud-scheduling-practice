package com.utm.entry.mapper;

import com.utm.entry.dto.SpaceDto;
import com.utm.entry.entity.Space;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SpaceMapper {
    SpaceMapper INSTANCE = Mappers.getMapper(SpaceMapper.class);

    Space dtoToEntity(SpaceDto SpaceDto);

    SpaceDto entityToDto(Space Space);
}
