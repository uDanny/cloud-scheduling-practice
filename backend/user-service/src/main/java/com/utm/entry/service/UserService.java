package com.utm.entry.service;

import com.utm.entry.dto.DefaultUserDto;
import com.utm.entry.dto.TeacherUser;
import com.utm.entry.entity.user.UserEntity;

import java.util.List;

public interface UserService {
    List<DefaultUserDto> getAllUserDetails();

    DefaultUserDto createUser(DefaultUserDto defaultUserDto);

    TeacherUser createTeacher(TeacherUser teacherUser);
}
