package com.utm.entry.service.impl;

import com.utm.entry.dto.UniversityDto;
import com.utm.entry.entity.University;
import com.utm.entry.mapper.UniversityMapper;
import com.utm.entry.repository.UniversityRepository;
import com.utm.entry.service.UniversityService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository universityRepository;
    private final UniversityMapper universityMapper = UniversityMapper.INSTANCE;

    public UniversityServiceImpl(UniversityRepository universityRepositor) {
        this.universityRepository = universityRepositor;
    }

    @Override
    public UniversityDto createUniversity(UniversityDto universityDto) {
        University university = universityRepository.save(universityMapper.dtoToEntity(universityDto));
        return universityMapper.entityToDto(university);
    }

    @Override
    public List<UniversityDto> getUniversities() {
        List<University> universities = universityRepository.findAll();
        return universityMapper.entitiesToDtos(universities);
    }
}
