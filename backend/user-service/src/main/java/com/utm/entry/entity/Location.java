package com.utm.entry.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table
@Data
public class Location {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "location_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column
    private String city;

    @Column
    private String region;

    @ManyToOne(fetch = FetchType.LAZY)
    private Organization organization;
}
