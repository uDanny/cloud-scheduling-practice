package com.utm.entry.entity.user.enums;

import lombok.Getter;

@Getter
public enum ROLE {
    ADMIN(Values.ADMIN),
    MANAGER(Values.MANAGER),
    EMPLOYER(Values.EMPLOYER),
    CLIENT(Values.CLIENT);

    public final String discriminatorValue;

    ROLE(String discriminatorValue) {
        if (!this.name().equals(discriminatorValue)) {
            throw new IllegalArgumentException("Wrong Role value");
        }
        this.discriminatorValue = discriminatorValue;
    }

    public static class Values {
        public static final String ADMIN = "ADMIN";
        public static final String MANAGER = "MANAGER";
        public static final String EMPLOYER = "EMPLOYER";
        public static final String CLIENT = "CLIENT";

    }
}
