package com.utm.entry.mapper;

import com.utm.entry.dto.TeacherUser;
import com.utm.entry.entity.user.EmployerDetails;
import com.utm.entry.entity.user.UserEntity;
import com.utm.entry.entity.user.enums.ROLE;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.LinkedList;

@Mapper(uses = UniversityMapper.class)
public interface EmployerMapper {
    EmployerMapper INSTANCE = Mappers.getMapper(EmployerMapper.class);

    @Mapping(source = "university", target = "university")
    UserEntity dtoToEntity(TeacherUser teacherUser);

    EmployerDetails dtoToDetailsEntity(TeacherUser teacherUser);

    @AfterMapping
    default void setDetUserDetailsByRole(TeacherUser teacherUser, @MappingTarget UserEntity userEntity) {
        userEntity.setUserDetails(new LinkedList<>(Collections.singletonList(dtoToDetailsEntity(teacherUser))));
    }

    TeacherUser entityToDto(UserEntity userEntity);

    @AfterMapping
    default void setDetUserDetailsByRole(UserEntity userEntity, @MappingTarget TeacherUser teacherUser) {
        EmployerDetails userDetails = (EmployerDetails) userEntity.getUserDetails().stream().filter(x -> ROLE.EMPLOYER == x.getRole()).findAny().get();
        teacherUser.setDescription(userDetails.getDescription());
        teacherUser.setAbleToModifyWorkTime(userDetails.isAbleToModifyWorkTime());
        teacherUser.setAbleToAddDaysOff(userDetails.isAbleToAddDaysOff());
    }

}
