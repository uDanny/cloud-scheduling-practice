package com.utm.entry.dto;


import lombok.Data;

@Data
public class DaysDto {
    private boolean MO;
    private boolean TU;
    private boolean WE;
    private boolean TH;
    private boolean FR;
    private boolean ST;
    private boolean SU;

}
