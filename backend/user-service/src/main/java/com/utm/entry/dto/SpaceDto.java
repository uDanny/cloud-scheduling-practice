package com.utm.entry.dto;

import lombok.Data;

@Data
public class SpaceDto {
    private int id;
    private String name;
}
