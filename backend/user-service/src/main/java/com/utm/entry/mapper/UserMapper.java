package com.utm.entry.mapper;

import com.utm.entry.dto.DefaultUserDto;
import com.utm.entry.dto.TeacherUser;
import com.utm.entry.entity.user.EmployerDetails;
import com.utm.entry.entity.user.UserDetails;
import com.utm.entry.entity.user.UserEntity;
import com.utm.entry.entity.user.enums.ROLE;
import com.utm.entry.utils.RoleMapperUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(uses = UniversityMapper.class)
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "university", target = "university")
    UserEntity dtoToEntity(DefaultUserDto defaultUserDto);

    @AfterMapping
    default void setDetUserDetailsByRole(DefaultUserDto defaultUserDto, @MappingTarget UserEntity userEntity) {
        userEntity.setUserDetails(RoleMapperUtils.getDefaultUserDetailsByRole(defaultUserDto));
    }

    @Mapping(source = "userDetails", target = "roles")
    DefaultUserDto entityToDto(UserEntity userEntity);

    List<DefaultUserDto> entitiesToDtos(List<UserEntity> userEntity);

    default List<ROLE> userDetailsToRoles(List<UserDetails> userDetails) {
        return userDetails.stream().map(UserDetails::getRole).collect(Collectors.toList());
    }
}
