package com.utm.entry.entity.user;


import com.utm.entry.entity.user.enums.ROLE;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue(value = ROLE.Values.EMPLOYER)
@Getter
@Setter
public class EmployerDetails extends UserDetails {
    @Column
    private String description;
    @Column
    private boolean ableToModifyWorkTime;
    @Column
    private boolean ableToAddDaysOff;

    public EmployerDetails() {
        super(ROLE.EMPLOYER);
    }
}
