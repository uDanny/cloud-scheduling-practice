package com.utm.entry.dto;


import lombok.Data;

@Data
public class TeacherUser extends DefaultUserDto {
    private String description;
    private boolean ableToModifyWorkTime;
    private boolean ableToAddDaysOff;
}
