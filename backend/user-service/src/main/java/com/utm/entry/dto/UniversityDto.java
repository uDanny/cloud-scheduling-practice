package com.utm.entry.dto;

import lombok.Data;

@Data
public class UniversityDto {
    private int id;
    private String name;
}
