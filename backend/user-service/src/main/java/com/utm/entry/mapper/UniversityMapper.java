package com.utm.entry.mapper;

import com.utm.entry.dto.UniversityDto;
import com.utm.entry.entity.University;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UniversityMapper {
    UniversityMapper INSTANCE = Mappers.getMapper(UniversityMapper.class);

    University dtoToEntity(UniversityDto UniversityDto);

    UniversityDto entityToDto(University University);

    List<UniversityDto> entitiesToDtos(List<University> University);
}
