package com.utm.entry.entity.user;

import com.utm.entry.entity.user.enums.ROLE;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Data
@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue(value = ROLE.Values.CLIENT)
public class ClientDetails extends UserDetails {

    @Column(nullable = false)
    private String groupName;

    public ClientDetails() {
        super(ROLE.CLIENT);
    }
}
