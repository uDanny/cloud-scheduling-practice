package com.utm.entry.mapper;

import com.utm.entry.dto.CourseDto;
import com.utm.entry.entity.Course;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CourseMapper {
    CourseMapper INSTANCE = Mappers.getMapper(CourseMapper.class);

    Course dtoToEntity(CourseDto courseDto);

    CourseDto entityToDto(Course course);
}
