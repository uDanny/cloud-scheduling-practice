package com.utm.entry.service;

import com.utm.entry.dto.UniversityDto;

import java.util.List;

public interface UniversityService {
    UniversityDto createUniversity(UniversityDto courseDto);

    List<UniversityDto> getUniversities();
}
