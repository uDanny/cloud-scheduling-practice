package com.utm.entry.entity.user;

import com.utm.entry.entity.University;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Data
public class UserEntity {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "user_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(nullable = false)
    private boolean isActive;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<UserDetails> userDetails;

    @OneToOne
    private University university;

}
