package com.utm.entry.repository;

import com.utm.entry.entity.Course;
import com.utm.entry.entity.user.ClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
}
