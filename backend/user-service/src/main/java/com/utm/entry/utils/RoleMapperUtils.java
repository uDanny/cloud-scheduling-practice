package com.utm.entry.utils;

import com.utm.entry.dto.DefaultUserDto;
import com.utm.entry.entity.user.AdminDetails;
import com.utm.entry.entity.user.EmployerDetails;
import com.utm.entry.entity.user.ManagerDetails;
import com.utm.entry.entity.user.UserDetails;
import com.utm.entry.entity.user.enums.ROLE;

import java.util.List;
import java.util.stream.Collectors;

public class RoleMapperUtils {
    public static List<UserDetails> getDefaultUserDetailsByRole(DefaultUserDto defaultUserDto) {
        return defaultUserDto.getRoles().stream().map(RoleMapperUtils::getDefaultUserDetailsByRole).collect(Collectors.toList());

    }

    private static UserDetails getDefaultUserDetailsByRole(ROLE role) {
        switch (role) {
            case ADMIN:
                return new AdminDetails();
            case MANAGER:
                return new ManagerDetails();
//            case EMPLOYER:
//                return new EmployerDetails();
//            case CLIENT:
//                return new ClientDetails();
            default:
                throw new IllegalArgumentException("Not recognized role");
        }
    }
}
