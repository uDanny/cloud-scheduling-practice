package com.utm.entry.service.impl;

import com.utm.entry.dto.CourseDto;
import com.utm.entry.entity.Course;
import com.utm.entry.mapper.CourseMapper;
import com.utm.entry.repository.CourseRepository;
import com.utm.entry.service.CourseService;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper = CourseMapper.INSTANCE;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public CourseDto createCourse(CourseDto courseDto) {
        Course course = courseRepository.save(courseMapper.dtoToEntity(courseDto));
        return courseMapper.entityToDto(course);
    }
}
