package com.utm.entry.service;

import com.utm.entry.dto.CourseDto;

public interface CourseService {
    CourseDto createCourse(CourseDto courseDto);
}
