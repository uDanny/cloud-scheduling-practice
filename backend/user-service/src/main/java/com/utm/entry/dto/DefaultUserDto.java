package com.utm.entry.dto;

import com.utm.entry.entity.user.enums.ROLE;
import lombok.Data;

import java.util.List;

@Data
public class DefaultUserDto {
    private int id;
    private String firstName;
    private String lastName;
    private List<ROLE> roles;
    private UniversityDto university;

}
