package com.utm.entry.controller;

import com.utm.entry.dto.*;
import com.utm.entry.service.CourseService;
import com.utm.entry.service.SpaceService;
import com.utm.entry.service.UniversityService;
import com.utm.entry.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {
    private final UserService userService;
    private final CourseService courseService;
    private final UniversityService universityService;
    private final SpaceService spaceService;

    public ApiController(UserService userService, CourseService courseService, UniversityService universityService, SpaceService spaceService) {
        this.userService = userService;
        this.courseService = courseService;
        this.universityService = universityService;
        this.spaceService = spaceService;
    }

    @GetMapping("/isAlive")
    public String getRoot() {
        return "Welcome from User API";
    }

    @GetMapping("/getAll")
    List<DefaultUserDto> getAllUserDetails() {
        return userService.getAllUserDetails();
    }

    @PostMapping("/course")
    public CourseDto createCourse(@RequestBody CourseDto courseDto) {
        return courseService.createCourse(courseDto);
    }

    @PostMapping("/university")
    UniversityDto createUniversity(@RequestBody UniversityDto courseDto) {
        return universityService.createUniversity(courseDto);
    }

    @PostMapping("/user")
    DefaultUserDto createUser(@RequestBody DefaultUserDto defaultUserDto) {
        return userService.createUser(defaultUserDto);
    }

    @GetMapping("/university")
    List<UniversityDto> getUniversities() {
        return universityService.getUniversities();
    }

    @PostMapping("/space")
    SpaceDto createSpace(@RequestBody SpaceDto spaceDto) {
        return spaceService.createSpace(spaceDto);
    }

    @PostMapping("/teacher")
    TeacherUser createTeacher(@RequestBody TeacherUser teacherUser) {
        return userService.createTeacher(teacherUser);
    }
}
