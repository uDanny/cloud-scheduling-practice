package com.utm.entry.entity.user;

import com.utm.entry.entity.user.enums.ROLE;
import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Data
@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue(value = ROLE.Values.ADMIN)
public class AdminDetails extends UserDetails {
    public AdminDetails() {
        super(ROLE.ADMIN);
    }
}
