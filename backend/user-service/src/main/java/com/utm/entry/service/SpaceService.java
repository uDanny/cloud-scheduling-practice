package com.utm.entry.service;

import com.utm.entry.dto.SpaceDto;

public interface SpaceService {
    SpaceDto createSpace(SpaceDto spaceDto);
}
