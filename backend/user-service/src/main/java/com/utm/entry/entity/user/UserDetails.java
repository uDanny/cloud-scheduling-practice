package com.utm.entry.entity.user;

import com.utm.entry.entity.user.enums.ROLE;
import lombok.Data;

import javax.persistence.*;

@Table
@Entity
@Data
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "role_details", discriminatorType = DiscriminatorType.STRING)
public abstract class UserDetails {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "user_details_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Column(name = "id", nullable = false)
    private Integer id;


    @Column(name = "role_details", nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private final ROLE role;

    public UserDetails(ROLE role) {
        this.role = role;
    }
}
