package com.utm.client.dto;


import lombok.Data;

import java.util.List;

@Data
public class UserDto {
    private int id;
    private String firstName;
    private String lastName;
    private UniversityDto university;
    private List<ROLE> roles;
}
