package com.utm.client.dto;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class TeacherUserDto extends UserDto {
    private String description;
    private LocalTime startWorkingTime;
    private LocalTime endWorkingTime;
    private LocalTime startBreakTime;
    private LocalTime endBreakTime;
    private LocalDate startScheduleDate;
    private LocalDate endScheduleDate;
    private boolean ableToModifyWorkTime;
    private boolean ableToAddDaysOff;
    private DaysDto days;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setStartWorkingTime(LocalDateTime startWorkingTime) {
        this.startWorkingTime = startWorkingTime.toLocalTime();
    }
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setEndWorkingTime(LocalDateTime endWorkingTime) {
        this.endWorkingTime = endWorkingTime.toLocalTime();
    }
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setStartBreakTime(LocalDateTime startBreakTime) {
        this.startBreakTime = startBreakTime.toLocalTime();
    }
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setEndBreakTime(LocalDateTime endBreakTime) {
        this.endBreakTime = endBreakTime.toLocalTime();
    }
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setStartScheduleDate(LocalDateTime startScheduleDate) {
        this.startScheduleDate = startScheduleDate.toLocalDate();
    }
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public void setEndScheduleDate(LocalDateTime endScheduleDate) {
        this.endScheduleDate = endScheduleDate.toLocalDate();
    }
}
