package com.utm.client.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.utm.client.dto.*;

import java.time.LocalDate;
import java.util.List;

public interface UserService {
    @HystrixCommand(
            fallbackMethod="getDefaultIngredients",
            commandProperties={
                    @HystrixProperty( name="circuitBreaker.requestVolumeThreshold", value="30"),
                    @HystrixProperty( name="circuitBreaker.errorThresholdPercentage", value="25"),
                    @HystrixProperty( name="metrics.rollingStats.timeInMilliseconds", value="20000"),
                    @HystrixProperty( name="circuitBreaker.sleepWindowInMilliseconds", value="50000")
            })
    String checkIsAlive();

    List<UserDto> getAllUsers();

    CourseDto createCourse(CourseDto courseDto);

    UniversityDto createUniversity(UniversityDto universityDto);

    UniversityDto createUser(UserDto userDto);

    List<UniversityDto> getUniversities();

    SpaceDto createSpace(SpaceDto spaceDto);

    TeacherUserDto createTeacher(TeacherUserDto teacherUserDto);

    TimeFrontDto getTeacherSchedule(int id, LocalDate localDate);
}
