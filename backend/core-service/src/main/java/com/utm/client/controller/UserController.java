package com.utm.client.controller;

import com.utm.client.dto.*;
import com.utm.client.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getAll")
    public List<UserDto> getUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/isAlive")
    public String checkIsAlive() {
        return userService.checkIsAlive();
    }

    @PostMapping("/course")
    public CourseDto createCourse(@RequestBody CourseDto courseDto) {
        return userService.createCourse(courseDto);
    }

    @PostMapping("/university")
    public UniversityDto createUniversity(@RequestBody UniversityDto universityDto) {
        return userService.createUniversity(universityDto);
    }

    @PostMapping("/manager")
    public UniversityDto createManager(@RequestBody UserDto userDto) {
        userDto.setRoles(Stream.of(ROLE.MANAGER).collect(Collectors.toList()));
        return userService.createUser(userDto);
    }

    @PostMapping("/space")
    public SpaceDto createManager(@RequestBody SpaceDto spaceDto) {
        return userService.createSpace(spaceDto);
    }

    @GetMapping("/university")
    public List<UniversityDto> getUniversities() {
        return userService.getUniversities();
    }

    @PostMapping("/teacher")
    public TeacherUserDto createTeacher(@RequestBody TeacherUserDto teacherUserDto) {
        teacherUserDto.setRoles(Collections.singletonList(ROLE.EMPLOYER));
        return userService.createTeacher(teacherUserDto);
    }
}
