package com.utm.client.dto;

import lombok.Data;

@Data
public class CourseDto {
    private int id;
    private String name;
}
