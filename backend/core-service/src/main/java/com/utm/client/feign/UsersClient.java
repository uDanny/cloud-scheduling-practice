package com.utm.client.feign;

import com.utm.client.dto.*;
import com.utm.client.dto.sendUser.TeacherUserSendDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@FeignClient("user-service")
public interface UsersClient {
    @GetMapping("/getAll")
//todo: review naming
    List<UserDto> getAllUsers();

    @GetMapping("/isAlive")
    String checkIsAlive();

    @PostMapping("/course")
    CourseDto createCourse(@RequestBody CourseDto courseDto);

    @PostMapping("/university")
    UniversityDto createUniversity(@RequestBody UniversityDto courseDto);

    @PostMapping("/user")
    UniversityDto createUser(UserDto userDto);

    @GetMapping("/university")
    List<UniversityDto> getUniversities();

    @PostMapping("/space")
    SpaceDto createSpace(SpaceDto spaceDto);

    @PostMapping("/teacher")
    TeacherUserSendDto createTeacher(TeacherUserSendDto teacherUserDto);
}
