package com.utm.client.dto.sendSchedule;

import com.utm.client.dto.DaysDto;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class TeacherTimeSendDto {
    private int id;
    private LocalTime startWorkingTime;
    private LocalTime endWorkingTime;
    private LocalTime startBreakTime;
    private LocalTime endBreakTime;
    private LocalDate startScheduleDate;
    private LocalDate endScheduleDate;
    private DaysDto days;
}
