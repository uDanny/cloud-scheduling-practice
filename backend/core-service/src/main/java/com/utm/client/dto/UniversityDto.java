package com.utm.client.dto;

import lombok.Data;

@Data
public class UniversityDto {
    private int id;
    private String name;
}
