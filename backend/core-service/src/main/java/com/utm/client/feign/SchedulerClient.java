package com.utm.client.feign;

import com.utm.client.dto.TimeDto;
import com.utm.client.dto.sendSchedule.TeacherTimeSendDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.time.LocalDateTime;


@FeignClient("scheduler-service")
public interface SchedulerClient {

    @PostMapping("/teacher")
    TeacherTimeSendDto createTeacherTime(@RequestBody TeacherTimeSendDto teacherTimeSendDto);

    @GetMapping("/getUserTime/{id}/{startDate}")
    TimeDto getTime(@PathVariable("id") int id, @PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate);
}
