package com.utm.client.dto;

import lombok.Data;

import java.util.List;

@Data
public class TimeDto {
    private List<DurationDto> durations;
    private DaysDto days;}
