package com.utm.client.service.impl;

import com.utm.client.dto.*;
import com.utm.client.dto.sendSchedule.TeacherTimeSendDto;
import com.utm.client.dto.sendUser.TeacherUserSendDto;
import com.utm.client.feign.SchedulerClient;
import com.utm.client.feign.UsersClient;
import com.utm.client.mapper.TeacherMapper;
import com.utm.client.service.UserService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UsersClient usersClient;
    private final SchedulerClient schedulerClient;
    TeacherMapper teacherMapper = TeacherMapper.INSTANCE;

    public UserServiceImpl(UsersClient usersClient, SchedulerClient schedulerClient) {
        this.usersClient = usersClient;
        this.schedulerClient = schedulerClient;
    }

    //    @HystrixCommand(
//            fallbackMethod="getDefaultIngredients",
//            commandProperties={
//                    @HystrixProperty( name="circuitBreaker.requestVolumeThreshold", value="30"),
//                    @HystrixProperty( name="circuitBreaker.errorThresholdPercentage", value="25"),
//                    @HystrixProperty( name="metrics.rollingStats.timeInMilliseconds", value="20000"),
//                    @HystrixProperty( name="circuitBreaker.sleepWindowInMilliseconds", value="50000")
//            })
    @Override
    public String checkIsAlive() {
        return usersClient.checkIsAlive();
    }

    @Override
    public List<UserDto> getAllUsers() {
        return usersClient.getAllUsers();
    }

    @Override
    public CourseDto createCourse(CourseDto courseDto) {
        return usersClient.createCourse(courseDto);
    }

    @Override
    public UniversityDto createUniversity(UniversityDto universityDto) {
        return usersClient.createUniversity(universityDto);
    }

    @Override
    public UniversityDto createUser(UserDto userDto) {
        return usersClient.createUser(userDto);
    }

    @Override
    public List<UniversityDto> getUniversities() {
        return usersClient.getUniversities();
    }

    @Override
    public SpaceDto createSpace(SpaceDto spaceDto) {
        return usersClient.createSpace(spaceDto);
    }

    @Override
    public TeacherUserDto createTeacher(TeacherUserDto teacherUserDto) {
        TeacherUserSendDto teacherUserSendDto = teacherMapper.dtoToUser(teacherUserDto);
        teacherUserSendDto = usersClient.createTeacher(teacherUserSendDto);

        teacherUserDto.setId(teacherUserSendDto.getId());
        TeacherTimeSendDto teacherTimeSendDto = teacherMapper.dtoToTime(teacherUserDto);
        schedulerClient.createTeacherTime(teacherTimeSendDto);
        return teacherMapper.userToDto(teacherUserSendDto);
    }

    @Override
    public TimeFrontDto getTeacherSchedule(int id, LocalDate startDate) {
        TimeDto timeDto = schedulerClient.getTime(id, startDate);

        TimeFrontDto timeFrontDto = new TimeFrontDto();
        timeFrontDto.setDays(timeDto.getDays());
        timeFrontDto.setDurations(timeDto.getDurations());

        return timeFrontDto;
    }

}
