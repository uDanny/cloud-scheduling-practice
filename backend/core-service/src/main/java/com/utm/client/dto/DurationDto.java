package com.utm.client.dto;

import lombok.Data;

import java.time.LocalTime;

@Data
public class DurationDto {
    private LocalTime start;
    private LocalTime end;
}
