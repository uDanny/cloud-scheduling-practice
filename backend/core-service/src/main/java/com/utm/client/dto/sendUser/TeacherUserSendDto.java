package com.utm.client.dto.sendUser;


import com.utm.client.dto.UserDto;
import lombok.Data;

@Data
public class TeacherUserSendDto extends UserDto {
    private String description;
    private boolean ableToModifyWorkTime;
    private boolean ableToAddDaysOff;
}
