package com.utm.client.mapper;

import com.utm.client.dto.TeacherUserDto;
import com.utm.client.dto.sendSchedule.TeacherTimeSendDto;
import com.utm.client.dto.sendUser.TeacherUserSendDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TeacherMapper {
    TeacherMapper INSTANCE = Mappers.getMapper(TeacherMapper.class);

    TeacherUserDto userToDto(TeacherUserSendDto teacherUserSendDto);

    TeacherUserSendDto dtoToUser(TeacherUserDto teacherUserDto);
    TeacherTimeSendDto dtoToTime(TeacherUserDto teacherUserSendDto);

}
