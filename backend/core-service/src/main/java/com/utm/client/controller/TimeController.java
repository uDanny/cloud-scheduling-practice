package com.utm.client.controller;

import com.utm.client.dto.TimeDto;
import com.utm.client.dto.TimeFrontDto;
import com.utm.client.service.UserService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "/teacher/time")
@CrossOrigin
public class TimeController {
    private final UserService userService;

    public TimeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/{startDate}")
    public TimeFrontDto getUniversities(@PathVariable("id") int id, @PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate) {
        return userService.getTeacherSchedule(id,startDate.toLocalDate() );
    }

}
