package com.utm.client.dto;

import lombok.Data;

import java.util.Comparator;
import java.util.List;

@Data
public class TimeFrontDto {
    private List<DurationDto> durations;
    private DaysDto days;
    private DurationDto extremeDuration;

    public void setDurations(List<DurationDto> durations) {
        this.durations = durations;
        DurationDto durationDto = new DurationDto();
        durationDto.setStart(durations.stream().map(DurationDto::getStart).min(Comparator.naturalOrder()).orElse(null));
        durationDto.setEnd(durations.stream().map(DurationDto::getEnd).max(Comparator.naturalOrder()).orElse(null));
        this.setExtremeDuration(durationDto);
    }

}
