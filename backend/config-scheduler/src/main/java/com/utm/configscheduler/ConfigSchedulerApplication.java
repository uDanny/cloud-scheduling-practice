package com.utm.configscheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigSchedulerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigSchedulerApplication.class, args);
    }

}
