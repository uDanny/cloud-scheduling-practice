# Spring Cloud practice

This is a demo project, as an proof of concept of the cloud architecture.
There are implemented technologies:
- Spring Cloud service discovery through Eureka server
- Spring Cloud config server with Git soursce of truth
- Hystrix service for Circuit Breaker pattern implementation
- Spring Admin service with codecentric UI
- Rest communication through FeignClient
