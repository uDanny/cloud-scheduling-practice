import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import FormItem from "antd/es/form/FormItem";

const {Drawer, Form, Button, Col, Row, Input, Icon} = antd;

class CreateUniversityComponent extends React.Component {
    state = {visible: false};

    university = {
        id: 0,
        name: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            university: this.university,
        };
        this.postUniversity = this.postUniversity.bind(this);
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };


    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Button type="primary" onClick={this.showDrawer}>
                    <Icon type="plus"/> New University
                </Button>
                <Drawer
                    title="Create a new University"
                    width={720}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    bodyStyle={{paddingBottom: 80}}
                >
                    <Form layout="vertical" hideRequiredMark onSubmit={this.handleSubmit}>
                        <Row gutter={16}>
                            <Col span={12}>
                                <Form.Item label="Name">
                                    {getFieldDecorator('name', {
                                        rules: [{required: true, message: 'Please enter University name'}],
                                    })(<Input placeholder="Please enter University name"/>)}
                                </Form.Item>
                            </Col>

                        </Row>
                        <FormItem>
                            <Button onClick={this.onClose} style={{marginRight: 8}}>
                                Cancel
                            </Button>
                            <Button htmlType="submit" onClick={this.onClose} type="primary">
                                Submit
                            </Button>
                        </FormItem>

                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >

                    </div>
                </Drawer>
            </div>
        );
    }

    async postUniversity() {
        const {university} = this.state;
        try {
            (await fetch('/users/university', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(university),
            })).json().then(data => {
                console.log("response data:" + data.id);
                this.university = data;
            });
        } catch (error) {
            console.error(error);
            this.setState({error});
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.university.id = 0;
                this.university.name = values.name;
                let university = this.university;
                this.setState({university});
                this.postUniversity()
            }
        });
    };
}

const WrappedCreateUniversityComponent = Form.create()(CreateUniversityComponent);
export default WrappedCreateUniversityComponent;
