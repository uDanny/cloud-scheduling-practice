import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';

const {Form, Col, Row,} = antd;

class StudentSchedulerComponent extends React.Component {
    teacherId = 0;
    today = new Date();
    dates = [];
    timeDto = {
        durations: [],
        extremeDuration: {
            start: null,
            end: null
        }
    };


    constructor(props) {
        super(props);
        this.teacherId = this.props.dataFromParent;

        // var startHour = new Date(2020, 1, 1, 7, 0, 0);

        // this.hoursOfDay.push({day: "SA", isToday: false, isWorkingDay: false, date: null});


        this.dates.push({day: "SU", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "MO", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "TU", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "WE", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "TH", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "FR", isToday: false, isWorkingDay: false, date: null});
        this.dates.push({day: "SA", isToday: false, isWorkingDay: false, date: null});

        let dayNumber = this.today.getDay();
        this.dates[dayNumber].isToday = true;

        for (let i = 0; i < this.dates.length; i++) {
            if (i < dayNumber) {
                this.dates[i].date = this.addDays(this.today, dayNumber - i)
            } else {
                this.dates[i].date = this.addDays(this.today, dayNumber + i)
            }
        }


        this.state = {
            today: this.today,
            dates: this.dates,
            teacherId: this.teacherId,
            timeDto: this.timeDto,
            isLoading: true
        };
    }

    addDays(date, days) {
        const copy = new Date(Number(date));
        copy.setDate(date.getDate() + days);
        return copy
    }

    componentDidMount() {
        this.setState({isLoading: true});

        fetch('http://localhost:8082/teacher/time/' + this.state.teacherId + "/" + this.state.dates[0].date.toISOString(), {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => response.json()
        ).then(data => this.setState({timeDto: data, isLoading: false}));
    }

    lala = function getUniversityOptions() {
        // return <div>{this.state.timeDto.extremeDuration.start}</div>
        return <div>ahooo</div>
    };
    //  heightFunction = (start, end) => {
    //     let startHour = start.substr(0, start.indexOf(':'));
    //     let startMinute = start.substr(start.indexOf(':'), start.length);
    //     return startHour * 60 + startMinute;
    // };
    //
    heightFunction = (start, end) => {
        if (!start || !end) {
            return '';
        }
        let startHour = start.substring(0, 2);
        let startMinute = start.substring(3, 5);
        let endHour = end.substring(0, 2);
        let endMinute = end.substring(3, 5);
        return ((endHour * 60 + endMinute) - (startHour * 60 + startMinute)) / 100;
    };


    render() {
        // const hoursOfDay = () => <div className="hoursOfDay">{this.state.timeDto.extremeDuration.start}</div>
        // const hoursOfDay = () => <div className="hoursOfDay">{this.getUniversityOptions}</div>


        // const heightFunction = (start, end) => {
        //     let startHour = start.substr(0, start.indexOf(':'));
        //     let startMinute = start.substr(start.indexOf(':'), start.length);
        //     return startHour * 60 + startMinute;
        // };

        // const hoursOfDay = <div className="hoursOfDay" style="" >{this.state.timeDto.extremeDuration.start}</div>;
        // const hoursOfDay = (day) => <div className="hoursOfDay">{day.day}</div>;
        const hoursOfDay = (day) => <div className="hoursOfDay"
                                         style={{height: this.heightFunction(this.state.timeDto.extremeDuration.start, this.state.timeDto.extremeDuration.end)}}
        >{this.heightFunction(this.state.timeDto.extremeDuration.start, this.state.timeDto.extremeDuration.end)}</div>;

        const daysOfWeeks = this.dates.map(eachDay =>
            <Col span={3}>
                <div className="dayContainer">
                    <div className="dayContainer">{eachDay.day}</div>
                    <div className="dayContainer">{eachDay.date.toDateString()}</div>
                    <div className="dayContainer">{hoursOfDay(eachDay)}</div>
                </div>
            </Col>
        );

        return (
            <div>
                <Row gutter={16}>
                    {(!this.state.isLoading) ? daysOfWeeks : ''}
                </Row>
            </div>
        );
    }
}

const WrappedStudentSchedulerComponent = Form.create()(StudentSchedulerComponent);
export default WrappedStudentSchedulerComponent;
