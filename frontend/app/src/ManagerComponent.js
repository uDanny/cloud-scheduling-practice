import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import AppNavbar from "./AppNavbar";
import WrappedCreateCourseComponent from "./CreateCourseComponent";
import WrappedCreateSpaceComponent from "./CreateSpaceComponent";
import WrappedCreateTeacherComponent from "./CreateTeacherComponent";

const {Drawer, Form, Button, Col, Row, Input, Icon} = antd;

class AdminComponent extends React.Component {


    render() {
        return (
            <div>
                <AppNavbar/>
                <WrappedCreateCourseComponent/>
                <WrappedCreateSpaceComponent/>
                <WrappedCreateTeacherComponent/>
            </div>
        );
    }


}

const WrappedAdminComponent = Form.create()(AdminComponent);
export default WrappedAdminComponent;
