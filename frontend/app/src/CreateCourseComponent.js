import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import FormItem from "antd/es/form/FormItem";

const {Drawer, Form, Button, Col, Row, Input, Icon} = antd;

class CreateCourseComponent extends React.Component {
    state = {visible: false};

    course = {
        id: 0,
        name: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            course: this.course,
        };
        this.postCourse = this.postCourse.bind(this);
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };


    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <Button type="primary" onClick={this.showDrawer}>
                    <Icon type="plus"/> New Course
                </Button>
                <Drawer
                    title="Create a new Course"
                    width={720}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    bodyStyle={{paddingBottom: 80}}
                >
                    <Form layout="vertical" hideRequiredMark onSubmit={this.handleSubmit}>
                        <Row gutter={16}>
                            <Col span={12}>
                                <Form.Item label="Name">
                                    {getFieldDecorator('name', {
                                        rules: [{required: true, message: 'Please enter Course name'}],
                                    })(<Input placeholder="Please enter Course name"/>)}
                                </Form.Item>
                            </Col>

                        </Row>
                        <FormItem>
                            <Button onClick={this.onClose} style={{marginRight: 8}}>
                                Cancel
                            </Button>
                            <Button htmlType="submit" onClick={this.onClose} type="primary">
                                Submit
                            </Button>
                        </FormItem>

                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >

                    </div>
                </Drawer>
            </div>
        );
    }

    async postCourse() {
        const {course} = this.state;
        try {
            (await fetch('/users/course', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(course),
            })).json().then(data => {
                console.log("response data:" + data.id);
                this.course = data;
            });
        } catch (error) {
            console.error(error);
            this.setState({error});
        }
        // this.props.history.push('/');
    }

    // componentDidUpdate() {
    // }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.course.id = 0;
                this.course.name = values.name;
                let course = this.course;
                this.setState({course});
                this.postCourse()
            }
        });
    };
}

const WrappedCreateCourseComponent = Form.create()(CreateCourseComponent);
export default WrappedCreateCourseComponent;
