import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import AppNavbar from "./AppNavbar";
import WrappedStudentSchedulerComponent from "./StudentSchedulerComponent";

const {Form} = antd;

class StudentComponent extends React.Component {
     render() {
        return (
            <div>
                <AppNavbar/>
                <WrappedStudentSchedulerComponent dataFromParent={this.props.match.params.id}/>
            </div>
        );
    }
}

const WrappedStudentComponent = Form.create()(StudentComponent);
export default WrappedStudentComponent;
