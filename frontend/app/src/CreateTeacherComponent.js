import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import FormItem from "antd/es/form/FormItem";
import moment from "moment";
import classNames from 'classnames'


const {Drawer, Form, Button, Col, Row, Input, Icon, TimePicker, Checkbox, DatePicker} = antd;
const {RangePicker} = DatePicker;
const timeFormat = 'HH:mm';
const dateFormat = 'YYYY/MM/DD';

class CreateTeacherComponent extends React.Component {
    state = {visible: false};

    teacher = {
        id: 0,
        firstName: '',
        lastName: '',
        description: '',
        startWorkingTime: moment.utc('08:00', timeFormat),
        endWorkingTime: moment.utc('17:00', timeFormat),

        startBreakTime: moment.utc('13:00', timeFormat),
        endBreakTime: moment.utc('14:00', timeFormat),

        startScheduleDate: moment.utc('2020/01/01', dateFormat),
        endScheduleDate: moment.utc('2020/02/01', dateFormat),

        ableToModifyWorkTime: false,
        ableToAddDaysOff: false,

        days: {
            "MO": false,
            "TU": false,
            "WE": false,
            "TH": false,
            "FR": false,
            "ST": false,
            "SU": false
        }

    };


    constructor(props) {
        super(props);
        this.state = {
            teacher: this.teacher,
        };
        this.postTeacher = this.postTeacher.bind(this);
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    onChangeStartTime = (time, timeString) => {
        //tpodo: parse timeString
        console.log(time, timeString);
        this.teacher.startWorkingTime = moment.utc(timeString, timeFormat);
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    onChangeEndTime = (time, timeString) => {
        //tpodo: parse timeString
        console.log(time, timeString);
        this.teacher.endWorkingTime = moment.utc(timeString, timeFormat);
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    onChangeStartTime = (time, timeString) => {
        //tpodo: parse timeString
        console.log(time, timeString);
        this.teacher.startBreakTime = moment.utc(timeString, timeFormat);
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    onChangeEndTime = (time, timeString) => {
        //tpodo: parse timeString
        console.log(time, timeString);
        this.teacher.endBreakTime = moment(timeString, timeFormat);
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    onChangeScheduledDate = (time, timeString) => {
        //tpodo: parse timeString
        console.log(time, timeString);
        this.teacher.startScheduleDate = moment.utc(timeString[0], dateFormat);
        this.teacher.endScheduleDate = moment.utc(timeString[1], dateFormat);
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    isAbleToModifyWorkTime = (e) => {
        //tpodo: parse timeString
        console.log(e.target.checked);
        this.teacher.ableToModifyWorkTime = e.target.checked;
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    isAbleToAddDaysOff = (e) => {
        console.log(e.target.checked);
        this.teacher.ableToAddDaysOff = e.target.checked;
        let tempTeacher = this.teacher;
        this.setState(tempTeacher);
    };

    handleMoClick = () => {
        this.teacher.days.MO = !this.teacher.days.MO;
        this.setState(this.teacher.days)
    };
    handleTuClick = () => {
        this.teacher.days.TU = !this.teacher.days.TU;
        this.setState(this.teacher.days)
    };
    handleWeClick = () => {
        this.teacher.days.WE = !this.teacher.days.WE;
        this.setState(this.teacher.days)
    };
    handleThClick = () => {
        this.teacher.days.TH = !this.teacher.days.TH;
        this.setState(this.teacher.days)
    };
    handleFrClick = () => {
        this.teacher.days.FR = !this.teacher.days.FR;
        this.setState(this.teacher.days)
    };
    handleStClick = () => {
        this.teacher.days.ST = !this.teacher.days.ST;
        this.setState(this.teacher.days)
    };
    handleSuClick = () => {
        this.teacher.days.SU = !this.teacher.days.SU;
        this.setState(this.teacher.days)
        let a = 0;
    };


    render() {
        const {getFieldDecorator} = this.props.form;


        return (
            <div>
                <Button type="primary" onClick={this.showDrawer}>
                    <Icon type="plus"/> New Teacher
                </Button>
                <Drawer
                    title="Create a new Teacher"
                    width={720}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    bodyStyle={{paddingBottom: 80}}
                >
                    <Form layout="vertical" hideRequiredMark onSubmit={this.handleSubmit}>
                        <Row gutter={16}>
                            <Col span={12}>
                                <Form.Item label="First Name">
                                    {getFieldDecorator('firstName', {
                                        rules: [{required: true, message: 'Please enter Teacher first name'}],
                                    })(<Input placeholder="Please enter Teacher first name"/>)}
                                </Form.Item>
                            </Col>

                            <Col span={12}>

                            </Col>
                            <Col span={12}>
                                <Form.Item label="Last Name">
                                    {getFieldDecorator('lastName', {
                                        rules: [{required: true, message: 'Please enter Teacher last name'}],
                                    })(<Input placeholder="Please enter Teacher last name"/>)}
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label="Description">
                                    {getFieldDecorator('description', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please enter Teacher description',
                                            },
                                        ],
                                    })(<Input.TextArea rows={4} placeholder="please enter Teacher description"/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={5}>
                                Working hours
                            </Col>
                            <Col span={5}>
                                <FormItem>
                                    <TimePicker defaultValue={moment('08:00 ', timeFormat)} format={timeFormat}
                                                onChange={this.onChangeStartTime}/>
                                </FormItem>
                            </Col>
                            <Col span={5}>
                                <FormItem>
                                    <TimePicker defaultValue={moment('17:00', timeFormat)} format={timeFormat}
                                                onChange={this.onChangeEndTime}/>
                                </FormItem>
                            </Col>
                        </Row>

                        <Row gutter={16}>
                            <Col span={5}>
                                Break hours
                            </Col>
                            <Col span={5}>
                                <FormItem>
                                    <TimePicker defaultValue={moment('13:00', timeFormat)} format={timeFormat}
                                                onChange={this.onChangeStartTime}/>
                                </FormItem>
                            </Col>
                            <Col span={5}>
                                <FormItem>
                                    <TimePicker defaultValue={moment('14:00', timeFormat)} format={timeFormat}
                                                onChange={this.onChangeEndTime}/>
                                </FormItem>
                            </Col>
                        </Row>

                        <Row gutter={16}>
                            <Col span={5}>
                                Schedule period
                            </Col>
                            <Col span={19}>
                                <FormItem>
                                    <RangePicker
                                        defaultValue={[moment('2020/01/01', dateFormat), moment('2020/02/01', dateFormat)]}
                                        format={dateFormat} onChange={this.onChangeScheduledDate}/>
                                </FormItem>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <FormItem>
                                    <Checkbox onChange={this.isAbleToModifyWorkTime}>Right to modify work
                                        time</Checkbox>
                                </FormItem>
                            </Col>
                        </Row>

                        <Row gutter={16}>
                            <Col span={24}>
                                <FormItem>
                                    <Checkbox onChange={this.isAbleToAddDaysOff}>Right to add days off</Checkbox>
                                </FormItem>
                            </Col>
                        </Row>

                        <Row gutter={16}>
                            <Col span={24}>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.MO && 'selectedDay')}
                                        onClick={this.handleMoClick}>MO
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.TU && 'selectedDay')}
                                        onClick={this.handleTuClick}>TU
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.WE && 'selectedDay')}
                                        onClick={this.handleWeClick}>WE
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.TH && 'selectedDay')}
                                        onClick={this.handleThClick}>TH
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.FR && 'selectedDay')}
                                        onClick={this.handleFrClick}>FR
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.ST && 'selectedDay')}
                                        onClick={this.handleStClick}>ST
                                </button>
                                <button type="button"
                                        className={classNames('daysOfWeek ', this.state.teacher.days.SU && 'selectedDay')}
                                        onClick={this.handleSuClick}>SU
                                </button>

                            </Col>
                        </Row>


                        <FormItem>
                            <Button onClick={this.onClose} style={{marginRight: 8}}>
                                Cancel
                            </Button>
                            <Button htmlType="submit" onClick={this.onClose} type="primary">
                                Submit
                            </Button>
                        </FormItem>

                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >

                    </div>
                </Drawer>
            </div>
        );
    }

    async postTeacher() {
        const {teacher} = this.state;
        try {
            (await fetch('/users/teacher', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(teacher),
            })).json().then(data => {
                console.log("response data:" + data.id);
                this.teacher = data;
            });
        } catch (error) {
            console.error(error);
            this.setState({error});
        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.teacher.id = 0;
                this.teacher.firstName = values.firstName;
                this.teacher.lastName = values.lastName;
                this.teacher.description = values.description;
                let teacher = this.teacher;
                this.setState({teacher});
                this.postTeacher()
            }
        });
    };
}

const WrappedCreateTeacherComponent = Form.create()(CreateTeacherComponent);
export default WrappedCreateTeacherComponent;
