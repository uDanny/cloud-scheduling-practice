import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import { DatePicker } from 'antd';

// import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
class LoginComponent extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div>
                        <h3>Welcome to Scheduler App</h3>
                        <Button color="success" tag={Link} to="/admin">Admin</Button>
                        <Button color="success" tag={Link} to="/manager">Manager</Button>
                        <Button color="success" tag={Link} to="/teacher/1">Teacher</Button>
                        <Button color="success" tag={Link} to="/student/1">Student</Button>
                    </div>
                </Container>
            </div>
        );
    }
}

export default LoginComponent;
