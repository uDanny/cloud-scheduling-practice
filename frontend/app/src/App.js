import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import LoginComponent from './LoginComponent';
import AdminComponent from "./AdminComponent";
import ManagerComponent from "./ManagerComponent";
import TeacherComponent from "./TeacherComponent";
import StudentComponent from "./StudentComponent";

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} component={LoginComponent}/>
                    <Route path='/admin' exact={true} component={AdminComponent}/>
                    <Route path='/manager' exact={true} component={ManagerComponent}/>
                    <Route path='/teacher/:id' exact={true} component={TeacherComponent}/>
                    <Route path='/student/:id' exact={true} component={StudentComponent}/>
                </Switch>
            </Router>
        )
    }
}

export default App;
