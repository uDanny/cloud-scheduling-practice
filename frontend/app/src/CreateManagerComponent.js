import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';

const {Drawer, Form, Button, Col, Row, Input, Icon, Select} = antd;
const {Option} = Select;


class CreateManagerComponent extends React.Component {
    state = {visible: false};

    manager = {
        id: 0,
        firstName: '',
        lastName: '',
        university: {
            id: 0,
            name: ''
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            manager: this.manager,
            universities: [],
            isLoading: true
        };
        //todo: remove this
        this.postManager = this.postManager.bind(this);
    }

    componentDidMount() {
        this.setState({isLoading: true});

        fetch('users/university', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => response.json()
        ).then(data => this.setState({universities: data, isLoading: false}));
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        // const {groups, isLoading} = this.state;

        return <div>
            <Button type="primary" onClick={this.showDrawer}>
                <Icon type="plus"/> New Manager
            </Button>
            <Drawer
                title="Create a new Manager"
                width={720}
                onClose={this.onClose}
                visible={this.state.visible}
                bodyStyle={{paddingBottom: 80}}
            >
                <Form layout="vertical" hideRequiredMark onSubmit={this.handleSubmit}>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="First Name">
                                {getFieldDecorator('firstName', {
                                    rules: [{required: true, message: 'Please enter First Name'}],
                                })(<Input placeholder="Please enter First Name"/>)}
                            </Form.Item>
                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="Last Name">
                                {getFieldDecorator('lastName', {
                                    rules: [{required: true, message: 'Please enter Last Name'}],
                                })(<Input placeholder="Please enter Last Name"/>)}
                            </Form.Item>
                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="University">
                                {getFieldDecorator('university', {
                                    rules: [{required: true, message: 'Please choose the University'}],
                                })(
                                    <Select placeholder="Please choose the University"
                                            loading={this.isLoading}>
                                        {this.getUniversityOptions()}
                                    </Select>,
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Form.Item>
                        <Button onClick={this.onClose} style={{marginRight: 8}}>
                            Cancel
                        </Button>
                        <Button htmlType="submit" onClick={this.onClose} type="primary">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
                <div
                    style={{
                        position: 'absolute',
                        right: 0,
                        bottom: 0,
                        width: '100%',
                        borderTop: '1px solid #e9e9e9',
                        padding: '10px 16px',
                        background: '#fff',
                        textAlign: 'right',
                    }}
                >


                </div>
            </Drawer>
        </div>;
    }

    getUniversityOptions() {
        return this.state.universities.map((x, y) => {
            return <Option key={x.id} value={y}>{x.name}</Option>
        })
    }

    async postManager() {
        const {manager} = this.state;
        try {
            //todo: check in case of error state is properly set
            (await fetch('/users/manager', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(manager),
            })).json().then(data => {
                console.log("response data:" + data.id);
                this.manager = data;
            });
        } catch (error) {
            console.error(error);
            this.setState({error});
        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.manager.id = 0;
                this.manager.firstName = values.firstName;
                this.manager.lastName = values.lastName;
                this.manager.university = this.state.universities[values.university];
                let manager = this.manager;
                this.setState({manager: manager});
                this.postManager()
            }
        });
    };
}

const
    WrappedCreateManagerComponent = Form.create()(CreateManagerComponent);
export default WrappedCreateManagerComponent;
