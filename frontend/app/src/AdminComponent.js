import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import AppNavbar from "./AppNavbar";
import WrappedCreateUniversityComponent from "./CreateUniversityComponent";
import WrappedCreateManagerComponent from "./CreateManagerComponent";

const {Form} = antd;

class AdminComponent extends React.Component {
    render() {
        return (
            <div>
                <AppNavbar/>
                <WrappedCreateUniversityComponent/>
                <WrappedCreateManagerComponent/>
            </div>
        );
    }
}

const WrappedAdminComponent = Form.create()(AdminComponent);
export default WrappedAdminComponent;
