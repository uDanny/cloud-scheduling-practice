import * as antd from "antd";
import React from "react";
import 'antd/dist/antd.css';
import AppNavbar from "./AppNavbar";
import WrappedTeacherSchedulerComponent from "./TeacherSchedulerComponent";

const {Form} = antd;

class TeacherComponent extends React.Component {
     render() {
        return (
            <div>
                <AppNavbar/>
                {/*<WrappedSchedulerComponent {...props} dataFromParent={this.props.match.params.id}/>*/}
                <WrappedTeacherSchedulerComponent dataFromParent={this.props.match.params.id}/>
            </div>
        );
    }
}

const WrappedTeacherComponent = Form.create()(TeacherComponent);
export default WrappedTeacherComponent;
